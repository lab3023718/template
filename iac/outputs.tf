output "bucket_arn" {
  description = "S3 Bucket ARN"
  value       = aws_s3_bucket.bucket.arn
}

output "bucket_id" {
  description = "S3 Bucket Id"
  value       = aws_s3_bucket.bucket.id
}

output "bucket_name" {
  description = "S3 Bucket Name"
  value       = aws_s3_bucket.bucket.bucket
}
