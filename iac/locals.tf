locals {

  resource_prefix = var.identifier
  bucket_name     = var.bucket_name
  tags = merge({
    Name = var.bucket_name,
    Identifier = var.identifier
  }, var.tags)

}
