variable "identifier" {
  description = "Identifier for the app pattern resource"
  type        = string
}

variable "bucket_name" {
  description = "Name for the S3 bucket"
  type        = string
}

variable "lifecycle_rules" {
  description = "Rules to use for the bucket lifecycle configuration"
  type        = any
  default     = []
}

variable "sse_algorithm" {
  description = "Server-side encryption configuration algorithm."
  type        = string
  default     = "aws:kms"
}

variable "tags" {
  description = "Tags which can be passed on to the AWS resources."
  type        = map(string)
  default     = {}
}

variable "versioning_status" {
  description = "Enable versioning on s3 bucket"
  type        = string
  default     = "Enabled"
}

